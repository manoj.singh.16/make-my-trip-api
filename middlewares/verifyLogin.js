const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  try {
    const { authorization } = req.headers;

    const token = authorization.split(" ")[1];

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (error, decoded) => {
      if (error) {
        res.status(401).json({ message: "unauthorized" });
      } else {
        req.user = decoded;
        next();
      }
    });
  } catch (error) {
    res.status(401).json({ message: "unauthorized" });
  }
};
