const express = require('express');
const cors = require('cors');
const config = require('./config');
require('dotenv').config();
const db = require('./db');
const logger = require('./middlewares/logger');

const usersRoute = require('./routes/usersRoute');
const airportsRoute = require('./routes/airportRoute');
const flightRoute = require('./routes/flightRoute');
const bookingRoute = require('./routes/bookingRoute');
const seatRoute = require('./routes/seatRoute');
const paymentRoute = require('./routes/paymentRoute');

const app = express();

app.use(cors());

app.use(logger);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.get('/', (req, res) => {
  res.send(process.env.NODE_ENV);
});

app.use('/api/users', usersRoute);
app.use('/api/airports', airportsRoute);
app.use('/api/flights', flightRoute);
app.use('/api/flights', bookingRoute);
app.use('/api/flights/booking', seatRoute);
app.use('/api/flights/booking/payment', paymentRoute);

app.use((req, res) => {
  res.status(404).json({ status: 404, message: 'not found' });
});

db.sync()
  .then(() => {
    console.log('DB connection has been established successfully.');

    const PORT = process.env.NODE_ENV === 'development' ? config.PORT : process.env.PORT;

    app.listen(PORT, () => {
      console.log(`server is running on port ${PORT}`);
    });
  })
  .catch((error) => console.log(error));
