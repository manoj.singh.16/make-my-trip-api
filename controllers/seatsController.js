const Booking = require('../models/Booking');
const Flight = require('../models/Flight');
const Seat = require('../models/Seat');

const seatNumber = async (req, res) => {
  try {
    const { id } = req.params;

    const bookingDetails = await Booking.findOne({
      where: {
        id,
      },
    });

    const flightId = bookingDetails.flight_id;

    const seats = await Seat.findOne({
      where: {
        flight_id: flightId,
      },
    });

    const allSeats = Object.entries(seats.dataValues)
      .filter((seat) => seat[0] !== 'flight_id' && seat[0] !== 'id')
      .map((seat) => {
        const seatNum = seat[0];
        const isconfirm = seat[1];

        return {
          seatNum,
          isconfirm,
        };
      });

    res.status(200).json({ flightId, allSeats });
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: 'server error' });
  }
};

module.exports = { seatNumber };
