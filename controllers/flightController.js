const { Op } = require('sequelize');
const Airline = require('../models/Airline');
const Flight = require('../models/Flight');
const moment = require('moment');
const Airport = require('../models/Airport');
const isEmpty = require('../utils/isEmpty');

module.exports = async (req, res) => {
  try {
    const { date, from, to, seats } = req.query;
    if (isEmpty(date) === false && isEmpty(from) === false && isEmpty(to) === false && isEmpty(seats) === false) {
      const data = await Flight.findAll({
        where: {
          [Op.and]: [
            {
              journey_date: {
                [Op.eq]: date,
              },
              from_airport: {
                [Op.eq]: from,
              },
              to_airport: {
                [Op.eq]: to,
              },
            },
          ],
        },
        include: { all: true },
      });

      const flights = data
        .filter((flight) => flight.booked_seats + parseInt(seats) <= flight.total_seats)

        .map((flight) => {
          const start = moment.utc(flight.dataValues.boarding_time, 'HH:mm');
          const end = moment.utc(flight.dataValues.arrival_time, 'HH:mm');

          // calculate the duration
          const d = moment.duration(end.diff(start));

          // format a string result

          flight.dataValues.duration = moment.utc(+d).format('HH:mm');

          return flight;
        });

      res.status(200).json(flights);
    } else {
      res
        .status(400)
        .json({ msg: `please provide "from airport", "to airport", "journey date" and "number of seats"'` });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: 'server error' });
  }
};
