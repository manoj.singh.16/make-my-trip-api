const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/User");
const signupValidation = require("../validations/signupValidation");
const generateOtp = require("../modules/generateOtp");
const config = require("../config");
const sendMail = require("../modules/sendMail");
const Otp = require("../models/Otp");
const signJwt = require("../modules/signJwt");

module.exports = async (req, res) => {
  try {
    const { isValid, error } = signupValidation(req.body);

    if (isValid) {
      const { email, password, name } = req.body;

      const user = await User.findOne({
        where: {
          email,
        },
      });

      console.log(user);

      if (user && user.isConfirmed === true) {
        error.email = "Email already exist";
      } else if (user) {
        const hashedPassword = await bcrypt.hash(password, config.saltRounds);
        user.name = name;
        user.password = hashedPassword;

        await user.save();

        const generatedOtp = generateOtp();

        const dbOtp = await Otp.findOne({
          where: {
            user_id: user.id,
          },
        });

        if (dbOtp) {
          dbOtp.otp = generatedOtp;

          await dbOtp.save();
        } else {
          await Otp.create({
            user_id: user.id,
            otp: generatedOtp,
          });
        }

        sendMail(email, generatedOtp);

        const { token } = await signJwt(
          {
            userId: user.id,
            name: user.name,
            email: user.email,
          },
          { expiresIn: config.otpTimeout }
        );

        return res.json({ otpToken: token });
      } else {
        const hashedPassword = await bcrypt.hash(password, config.saltRounds);
        const newUser = await User.create({
          name: name,
          password: hashedPassword,
          email: email,
        });

        const generatedOtp = generateOtp();

        await Otp.create({
          user_id: newUser.id,
          otp: generatedOtp,
        });
        sendMail(email, generatedOtp);

        const { token } = await signJwt(
          {
            userId: newUser.id,
            name: newUser.name,
            email: newUser.email,
          },
          { expiresIn: config.otpTimeout }
        );

        return res.json({ message: "OTP Sent", otpToken: token });
      }
    }

    res.status(400).json({ message: "Validation Failed", error });
  } catch (error) {
    console.log(error);
    res.status(500).json(error);
  }
};
