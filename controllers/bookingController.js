const Booking = require('../models/Booking');
const Flight = require('../models/Flight');
const Ticket = require('../models/Ticket');
const isEmpty = require('../utils/isEmpty');

const createBooking = async (req, res) => {
  try {
    const { flightId, numPassengers } = req.body;
    const userId = req.user.userId;
    if (!isEmpty(flightId) && !isEmpty(numPassengers) && !isEmpty(userId)) {
      const flightDetails = await Flight.findOne({
        where: {
          id: flightId,
        },
        include: { all: true },
      });

      const basePrice = flightDetails.price_per_seat;

      const total = parseFloat(basePrice) * parseInt(numPassengers);

      const payable = total * (1 + 0.05);

      const totalTax = payable - total;

      newBooking = await Booking.create({
        user_id: userId,
        flight_id: flightId,
        num_passengers: numPassengers,
        base_price: basePrice,
        total: total,
        tax: totalTax,
        payable_with_tax: payable,
      });

      res.status(200).json({ newBooking, flightDetails });
    } else {
      res.status(400).json({ msg: 'Invalid Request' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: 'server error' });
  }
};

const getBookingById = async (req, res) => {
  try {
    const { id } = req.params;

    const booking = await Booking.findOne({
      where: {
        id,
      },
    });

    if (booking.user_id !== req.user.userId) {
      return res.status(401).json({ message: 'unauthorized' });
    } else {
      const flightId = booking.flight_id;

      const flightDetails = await Flight.findOne({
        where: {
          id: flightId,
        },
        include: { all: true },
      });

      res.status(200).json({ getBooking: booking, flightDetails });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ msg: 'server error' });
  }
};

const bookPassengers = async (req, res) => {
  try {
    const { bookingId, passengers } = req.body;

    const bookingData = await Booking.findOne({ where: { id: bookingId } });

    if (bookingData.user_id !== req.user.userId) {
      return res.status(401).json({ message: 'unauthorized' });
    }

    const tickets = await passengers.map(async (passenger) => {
      const ticketForPassenger = await Ticket.create({
        user_id: req.user.userId,
        booking_id: bookingId,
        first_name: passenger.firstName,
        last_name: passenger.lastName,
        gender: passenger.gender,
      });

      return ticketForPassenger;
    });

    res.status(200).json({ msg: 'Passenger detail saved' });
  } catch (error) {
    console.log(error);
    res.status(500).json({ msg: 'server error' });
  }
};

module.exports = { createBooking, getBookingById, bookPassengers };
