const { Op } = require('sequelize');
const { findAll } = require('../models/Airport');
const Airport = require('../models/Airport');
const isEmpty = require('../utils/isEmpty');

module.exports = async (req, res) => {
  try {
    const { query } = req.query;

    if (!isEmpty(query)) {
      const data = await Airport.findAll({
        where: {
          [Op.or]: [
            {
              name: {
                [Op.like]: `%${query}%`,
              },
            },
            {
              city: {
                [Op.like]: `%${query}%`,
              },
            },
            {
              country: {
                [Op.like]: `%${query}%`,
              },
            },
            {
              iata: {
                [Op.like]: `%${query}%`,
              },
            },
          ],
        },
      });

      res.status(200).json(data);
    } else {
      const data = await Airport.findAll();

      res.status(200).json(data);
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ msg: 'server error' });
  }
};
