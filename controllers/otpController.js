const jwt = require("jsonwebtoken");

const Otp = require("../models/Otp");
const User = require("../models/User");

const loginUser = require("../modules/loginUser");

module.exports = async (req, res) => {
  const otp = req.body.otp;
  const userId = req.user.userId;

  if (otp) {
    const dbOtp = await Otp.findOne({
      where: {
        user_id: userId,
      },
    });

    if (otp === dbOtp.otp) {
      const user = await User.findOne({
        where: {
          id: userId,
        },
      });

      user.isConfirmed = true;

      await user.save();

      const { token } = loginUser({
        userId: user.id,
        email: user.email,
        name: user.name,
      });
      return res.json({ message: "success", accessToken: token });
    }
  }
  res.status(400).json({
    message: "failed",
    error: {
      otp: "Invalid OTP",
    },
  });
};
