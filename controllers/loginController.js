const loginValidation = require("../validations/loginValidation");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const loginUser = require("../modules/loginUser");

module.exports = async (req, res) => {
  const { isValid, error } = loginValidation(req.body);

  if (!isValid) {
    return res.status(400).json({ message: "Invalid Credentials", error });
  }

  const { email, password } = req.body;

  const dbUser = await User.findOne({
    where: {
      email,
      isConfirmed: true,
    },
  });

  if (!dbUser) {
    return res.status(400).json({ message: "No user with the provided email" });
  }

  const isMatch = await bcrypt.compare(password, dbUser.password);

  if (!isMatch) {
    return res.status(400).json({ message: "Incorrect password" });
  }

  const { token } = loginUser({
    userId: dbUser.id,
    email: dbUser.email,
    name: dbUser.name,
  });

  res.json({ message: "success", accessToken: token });
};
