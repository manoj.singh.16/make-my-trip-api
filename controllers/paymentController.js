const Booking = require('../models/Booking');
const razorpay = require('../razorPay');
const shortid = require('shortid');

module.exports = async (req, res) => {
  try {
    const { id } = req.params;

    const { userId } = req.user;

    const bookingDetails = await Booking.findOne({
      where: {
        id,
      },
    });

    if (bookingDetails.user_id === userId) {
      const payment_capture = 1;
      const amount = bookingDetails.payable_with_tax;
      const currency = 'INR';

      const options = {
        amount: amount * 100,
        currency,
        receipt: shortid.generate(),
        payment_capture,
      };

      const response = await razorpay.orders.create(options);

      res.status(200).json({
        id: response.id,
        currency: response.currency,
        amount: response.amount,
      });
    } else {
      return res.status(401).json({ message: 'unauthorized' });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ msg: 'server error' || error.msg });
  }
};
