-- SQL DDL queries

CREATE TABLE "countries" (
  "id" INT PRIMARY KEY,
  "code" VARCHAR(3),
  "name" VARCHAR(200)
);

CREATE TABLE "airline" (
  "id" INT PRIMARY KEY,
  "name" VARCHAR(200)
);

CREATE TABLE "passengers" (
  "id" INT PRIMARY KEY,
  "name" VARCHAR(200),
  "gender" VARCHAR(10),
  "email" VARCHAR(200),
  "phone" TEXT,
  "birthDate" DATE
);

CREATE TABLE "user" (
  "id" INT PRIMARY KEY,
  "name" VARCHAR(200),
  "password_hash" VARCHAR(300),
  "email" VARCHAR(200),
  "phone_number" Text,
  "gender" VARCHAR(10),
  "birthdate" DATE,
  "is_active" BOOLEAN
);

CREATE TABLE "airports" (
  "airport_id" INT PRIMARY KEY,
  "longitude" FLOAT,
  "airport_name" VARCHAR(200),
  "lattitude" FLOAT,
  "city" VARCHAR(100),
  "country" INT ,
  CONSTRAINT "FK_airports.country"
    FOREIGN KEY ("country")
      REFERENCES "countries"("id")
);

CREATE TABLE "flights" (
  "id" Int PRIMARY KEY,
  "airline_id" INT,
  "economy_price" NUMERIC(9,2),
  "premium_price" NUMERIC(9,2),
  "business_price" NUMERIC(9,2),
  "to" INT,
  "from" INT,
  "arrival_time" TIMESTAMP WITH TIME ZONE,
  "departure_time" TIMESTAMP WITH TIME ZONE,
  "baggage" VARCHAR(10),
  "check_in" FLOAT,
  "cabin" FLOAT,
  "status" VARCHAR(10),
  CONSTRAINT "FK_flights.airline_id"
    FOREIGN KEY ("airline_id")
      REFERENCES "airline"("id"),
  CONSTRAINT "FK_flights.to"
    FOREIGN KEY ("to")
      REFERENCES "airports"("airport_id"),
  CONSTRAINT "FK_flights.from"
    FOREIGN KEY ("from")
      REFERENCES "airports"("airport_id")
);



CREATE TABLE "bookings" (
  "booking_id" INT PRIMARY KEY,
  "user_id" INT,
  "flight_id" INT,
  "transaction_id" VARCHAR(10),
  CONSTRAINT "FK_bookings.user_id"
    FOREIGN KEY ("user_id")
      REFERENCES "user"("id"),
  CONSTRAINT "FK_bookings.flight_id"
    FOREIGN KEY ("flight_id")
      REFERENCES "flights"("id")
);

CREATE TABLE "ticket" (
    "id" INT PRIMARY KEY,
    "booking_id" INT,
    "passenger_id" INT,
    "status" VARCHAR(10),
    CONSTRAINT "FK_ticket.passenger_id"
        FOREIGN KEY ("passenger_id")
        REFERENCES "passengers"("id"),
    CONSTRAINT "FK_ticket.booking_id"
        FOREIGN KEY ("booking_id")
        REFERENCES "bookings"("booking_id")
);

