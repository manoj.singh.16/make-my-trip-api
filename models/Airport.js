const Sequelize = require('sequelize');
const db = require('../db');
const Flight = require('./Flight');

const Airport = db.define(
  'airport',
  {
    name: {
      type: Sequelize.STRING,
    },
    city: {
      type: Sequelize.STRING,
    },
    country: {
      type: Sequelize.STRING,
    },
    iata: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: false }
);

Flight.belongsTo(Airport, { as: 'toAirport', foreignKey: 'to_airport' });
Flight.belongsTo(Airport, { as: 'fromAirport', foreignKey: 'from_airport' });

module.exports = Airport;
