const Sequelize = require('sequelize');
const db = require('../db');
const Flight = require('./Flight');

const Seat = db.define(
  'seats',
  {
    flight_id: {
      type: Sequelize.INTEGER,
    },
    a01: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a02: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a03: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a04: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a05: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a06: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a07: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a08: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a09: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a10: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    a11: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b01: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b02: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b03: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b04: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b05: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b06: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b07: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b08: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b09: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b10: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    b11: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c01: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c02: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c03: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c04: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c05: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c06: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c07: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c08: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c09: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    c10: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    c11: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d01: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d02: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d03: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d04: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d05: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d06: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d07: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d08: {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    },
    d09: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d10: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
    d11: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  },
  { timestamps: false }
);

Seat.belongsTo(Flight, { foreignKey: 'flight_id' });

module.exports = Seat;
