const Sequelize = require('sequelize');
const db = require('../db');

const Booking = db.define('bookings', {
  user_id: {
    type: Sequelize.INTEGER,
  },
  flight_id: {
    type: Sequelize.INTEGER,
  },
  num_passengers: {
    type: Sequelize.INTEGER,
  },
  base_price: {
    type: Sequelize.FLOAT,
  },
  tax: {
    type: Sequelize.FLOAT,
  },
  total: {
    type: Sequelize.FLOAT,
  },
  is_confirm: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  payable_with_tax: {
    type: Sequelize.FLOAT,
  },
  createdAt: {
    field: 'created_at',
    type: Sequelize.DATE,
  },
  updatedAt: {
    field: 'updated_at',
    type: Sequelize.DATE,
  },
});

module.exports = Booking;
