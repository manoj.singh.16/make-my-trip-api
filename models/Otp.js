const Sequelize = require("sequelize");
const db = require("../db");

const Otp = db.define(
  "otp",
  {
    user_id: {
      type: Sequelize.INTEGER,
    },
    otp: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: true }
);

module.exports = Otp;
