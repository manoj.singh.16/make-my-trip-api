const Sequelize = require("sequelize");
const db = require("../db");
const Otp = require("./Otp");

const User = db.define(
  "user",
  {
    name: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    isConfirmed: {
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    },
  },
  { timestamps: true }
);

User.hasOne(Otp, { foreignKey: "user_id" });
Otp.belongsTo(User, { foreignKey: "user_id" });

module.exports = User;
