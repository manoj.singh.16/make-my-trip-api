const Sequelize = require('sequelize');
const db = require('../db');
const Booking = require('./Booking');
const User = require('./User');

const Ticket = db.define('tickets', {
  user_id: {
    type: Sequelize.INTEGER,
  },
  booking_id: {
    type: Sequelize.INTEGER,
  },
  first_name: {
    type: Sequelize.STRING,
  },
  last_name: {
    type: Sequelize.STRING,
  },
  gender: {
    type: Sequelize.ENUM('M', 'F'),
  },
  seat_num: {
    type: Sequelize.STRING,
  },
  is_confirm: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

Ticket.belongsTo(Booking, { foreignKey: 'booking_id' });
Ticket.belongsTo(User, { foreignKey: 'user_id' });

module.exports = Ticket;
