const Sequelize = require('sequelize');
const db = require('../db');
const Flight = require('./Flight');

const Airline = db.define(
  'airlines',
  {
    iconurl: {
      type: Sequelize.STRING,
    },
    name: {
      type: Sequelize.STRING,
    },
  },
  { timestamps: false }
);

Airline.hasOne(Flight, { foreignKey: 'airline_id' });
Flight.belongsTo(Airline, { foreignKey: 'airline_id' });

module.exports = Airline;
