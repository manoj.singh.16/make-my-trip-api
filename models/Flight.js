const Sequelize = require('sequelize');
const db = require('../db');
const Booking = require('./Booking');
const User = require('./User');

const Flight = db.define(
  'flights',
  {
    airline_id: {
      type: Sequelize.INTEGER,
    },
    boarding_time: {
      type: Sequelize.TIME,
    },
    arrival_time: {
      type: Sequelize.TIME,
    },
    price_per_seat: {
      type: Sequelize.INTEGER,
    },
    from_airport: {
      type: Sequelize.INTEGER,
    },
    to_airport: {
      type: Sequelize.INTEGER,
    },
    journey_date: {
      type: Sequelize.DATEONLY,
    },
    total_seats: {
      type: Sequelize.INTEGER,
    },
    booked_seats: {
      type: Sequelize.INTEGER,
    },
  },
  { timestamps: false }
);

Booking.belongsTo(User, { foreignKey: 'user_id' });
Booking.belongsTo(Flight, { foreignKey: 'flight_id' });

module.exports = Flight;
