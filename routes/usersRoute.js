const express = require("express");

const User = require("../models/User");
const signupRoute = require("./signupRoute");
const loginRoute = require("./loginRoute");
const verifyLogin = require("../middlewares/verifyLogin");

const router = express.Router();

// GET - /api/users
router.get("/", async (req, res) => {
  try {
    const users = await User.findAll();

    res.json(users);
  } catch (error) {
    console.log(error);
  }
});

router.use("/signup", signupRoute);

router.use("/login", loginRoute);

router.get("/verify", verifyLogin, (req, res) => {
  res.json(req.user);
});

module.exports = router;
