const express = require("express");
const loginController = require("../controllers/loginController");

const router = express.Router();

// /api/users/login/
router.post("/", loginController);

module.exports = router;
