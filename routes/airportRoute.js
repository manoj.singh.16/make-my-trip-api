const airportController = require('../controllers/airportController');
const express = require('express');
const router = express.Router();

//Get
router.get('/', airportController);

module.exports = router;
