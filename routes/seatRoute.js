const express = require('express');
const { seatNumber } = require('../controllers/seatsController');
const verifyLogin = require('../middlewares/verifyLogin');
const router = express.Router();

//GET
router.get('/seat/:id', seatNumber);

module.exports = router;
