const express = require("express");

const otpController = require("../controllers/otpController");
const signupController = require("../controllers/signupController");
const verifyOtpJwt = require("../middlewares/verifyOtpJwt");

const router = express.Router();

// POST - /api/users/signup
router.post("/", signupController);

router.post("/verify-otp", verifyOtpJwt, otpController);

module.exports = router;
