const express = require('express');
const { createBooking, getBookingById, bookPassengers } = require('../controllers/bookingController');
const verifyLogin = require('../middlewares/verifyLogin');
const router = express.Router();

//post
router.post('/booking', verifyLogin, createBooking);
router.get('/booking/:id', verifyLogin, getBookingById);
router.post('/booking/ticket', verifyLogin, bookPassengers);

module.exports = router;
