const flightController = require('../controllers/flightController');

const express = require('express');
const router = express.Router();

//Get
router.get('/', flightController);

module.exports = router;
