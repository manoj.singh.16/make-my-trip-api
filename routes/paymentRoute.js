const express = require('express');
const paymentController = require('../controllers/paymentController');
const verifyLogin = require('../middlewares/verifyLogin');
const router = express.Router();

//POST
router.post('/:id', verifyLogin, paymentController);

module.exports = router;
