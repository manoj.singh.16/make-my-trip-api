const config = {
  PORT: 4000,
  saltRounds: 10,
  accessTimeout: 3600,
  otpTimeout: 60,
};

module.exports = config;
