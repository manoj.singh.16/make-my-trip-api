const jwt = require("jsonwebtoken");

module.exports = (payload, option) => {
  return new Promise((resolve, resject) => {
    jwt.sign(payload, process.env.OTP_TOKEN_SECRET, option, (err, token) => {
      if (err) {
        resject({ message: "internal server error" });
      }

      resolve({ token });
    });
  });
};
