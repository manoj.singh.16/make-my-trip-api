const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = async (to, otp) => {
  const msg = {
    to,
    from: "manoj.singh.16@mountblue.tech", // Use the email address or domain you verified above
    subject: "Your OTP for Make My Trip",
    fromname: "Make My Trip",
    html: `<p>One Time Password for Make My Trip is <strong>${otp}</strong></p>`,
  };

  try {
    await sgMail.send(msg);
  } catch (error) {
    console.error(error);

    if (error.response) {
      console.error(error.response.body);
    }
  }
};
