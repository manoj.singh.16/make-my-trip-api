const jwt = require("jsonwebtoken");
const config = require("../config");
const signJwt = require("../modules/signJwt");

module.exports = (user) => {
  const token = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, {
    expiresIn: 3600,
  });

  return { token };
};
