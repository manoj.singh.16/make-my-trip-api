const validator = require("validator");
const isEmpty = require("../utils/isEmpty");

module.exports = (data) => {
  const { email, password } = data;

  const error = {};

  if (!validator.isEmail(email)) {
    error.email = "Invalid Email";
  }

  if (isEmpty(email)) {
    error.email = "email is required";
  }

  if (isEmpty(password)) {
    error.password = "Password is required";
  }

  return { error, isValid: isEmpty(error) };
};
