const validator = require("validator");
const isEmpty = require("../utils/isEmpty");

module.exports = (data) => {
  const { name, email, password, password2 } = data;

  const error = {};
  if (!validator.isEmail(email)) {
    error.email = "Invalid Email";
  }

  if (isEmpty(email)) {
    error.email = "email is required";
  }

  if (!validator.isLength(name, { min: 3, max: 50 })) {
    error.name = "name must be between 4 to 50 characters long";
  }

  if (isEmpty(name)) {
    error.name = "Username is required";
  }

  if (!validator.isLength(password, { min: 6, max: 30 })) {
    error.password = "Password must be between 6 to 30 characters long";
  }
  if (isEmpty(password)) {
    error.password = "Password is required";
  }

  if (!validator.equals(password, password2)) {
    error.password2 = "Passwords do not match";
  }

  if (isEmpty(password2)) {
    error.password2 = "Password confirmation is required";
  }

  return { error, isValid: isEmpty(error) };
};
